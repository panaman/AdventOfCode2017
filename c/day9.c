#include <stdio.h>
#include <stdlib.h>

int florb(char*);

int main()
{
	FILE* input = fopen("day9.txt", "r");
	fseek(input, 0, SEEK_END);
	const int size = ftell(input);
	rewind(input);

	char* stream = malloc(size);
	int pos = 0;
	char cur, temp;
	int count = 0;
	while ((cur = fgetc(input)) != '\n') {
		switch (cur) {
		case '!':
			fgetc(input);
			continue;
		case '<':
			while((temp = fgetc(input)) != '>') {
				if (temp == '!')
					fgetc(input);
				else
					count++;
			}
			continue;
		default:
			stream[pos++] = cur;
		}
	}
	stream[pos] = 0;
	fclose(input);

	// stream "cleaned", no more garbage/negations
	int score = florb(stream);
	printf("%d\n", score);
	printf("%d\n", count);

	free(stream);
}

int
florb(char* str)
{
	int ans = 0;
	int pos = 0;
	int level = 0;
	while (str[pos]) {
		switch (str[pos++]) {
		case '{':
			ans += ++level;
			continue;
		case '}':
			level--;
		}
	}
	return ans;
}

